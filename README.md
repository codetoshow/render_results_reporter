Ken Mohamed

# Solution Description

## Folder Structure
The solution is in a directory named "render_results_reporter".

Once installed and run (see below), the `run.sh` bash script calls `src/bin/render_report.py`; this module
preps the runtime environment and invokes `src/launcher.launch`.
  
`src/cli_handler.py` is a helper module used by `src/bin/render_report.py` to parse the command-line
and get the cli-args to `src/launcher.launch`.

The launcher initiates the assembly of the runtime, then calls the
`execute` method of the `src/core.RenderResultsReporter` instance.

`src/core.RenderResultsReporter` is a Command-object that handles all the processing for the solution.

The `src/configuration_manager.py` provides a means to access the 
preferences, `etc/render_report_configuration.yml`.

```
render_results_reporter
├── Makefile
├── README.md
├── etc
│   └── render_report_configuration.yml
├── run.sh
└── src
    ├── bin
    │   └── render_report.py
    ├── builder
    │   ├── __init__.py
    │   ├── default_configurator.py
    │   └── use_case_builder.py
    ├── business_rules.py
    ├── cli_handler.py
    ├── configuration_manager.py
    ├── core.py
    ├── launcher.py
    └───render_data_reader
        ├── __init__.py
        ├── csv_reader.py
        └── reader_interface.py
```

## Installation
 
cd to the "render_results_reporter" directory

>\>  make

Uninstall:

>\>  make clean

 * Installation location:`/usr/local`
 * Installation folder:`/usr/local/render_results_reporter`
 * Solution Executable:`/usr/local/bin/run.sh`


## Running the Solution
Examples:

From `render_results_reporter/data/render_results`
>\>  run.sh

From `render_results_reporter`
>\> run.sh data/render_results

### Sample Run
>|=> ./run.sh -sf -d data/render_results
>
>There were 47 successful renders
>
>Average Render Time: 5831 seconds.
>
>Average Peak-CPU: 85%.
>
>Average RAM Usage: 10019 MB.
>
>Max Peak-RAM Render-ID: 4727
>
>Max Peak-CPU Render-ID: 4307`

