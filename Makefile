.DEFAULT_GOAL := default

build:
	@mkdir -p -m 0777 build/render_report
	@cp -a ./src ./build/render_report/src
	@cp -a ./etc ./build/render_report/etc
	@cp -a ./run.sh ./build/render_report 2>/dev/null
		
install:
	@cp -a build/render_report /usr/local
	@ln -s /usr/local/render_report/run.sh /usr/local/bin/run.sh 2>/dev/null
	

clean: uninstall
	@rm -rf build
	

uninstall:
	@rm -f /usr/local/bin/run.sh
	@rm -rf /usr/local/render_report

		
ok:
	@echo "...OK"
	
	
default: build install ok

