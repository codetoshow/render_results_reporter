"""PConfiguration Manager"""
# STANDARD IMPORTS
import os
import re
import sys
import yaml



def get_preferences():
    with open(os.environ["PACKAGE_CONFIG"]) as f:
        data = f.read()
    return yaml.load(data, Loader=yaml.Loader)

