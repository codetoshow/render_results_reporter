"""
Use-Case Builder

Implements the Builder pattern to allow multiple use-case configurations
"""
# LOCAL IMPORTS
from core import RenderResultsReporter


class UseCaseAssembler(object):
    """
    Creates and instance of the Use-Case-Controller (using self._builder,
    which gets its value by constructor-dependency-injection)
    Performs the setter-dependency-injection for the UC-Controller

    ("Director/Controller" of the Builder Pattern)
    """
    def __init__(self,
                 builder=None,
                 cli_args=None,
                 prefs=None,
                 business_rules=None,
                 reader=None,
                 ):
        self._builder = builder()

        self._cli_args = cli_args
        self._business_rules = business_rules()
        self._reader = reader(prefs)

    def _assemble_use_case(self):
        """
        Performs the use-case assembly using a use-case-specific builder
        ... calls all the setter methods of the concrete builder
        """
        self._builder.create_use_case()

        self._builder.set_cli_args(self._cli_args)
        self._builder.set_business_rules(self._business_rules)
        self._builder.set_reader(self._reader)
        self._builder.set_render_data_directory(self._cli_args.render_results_directory)

    def get_use_case(self):
        """Assembles and returns the UC-Controller"""
        self._assemble_use_case()
        return self._builder.use_case


class UseCaseBuilder(object):
    """
    Provides the interface for building the Object
    Implements Constructor Dependency Injection for required and default fields
    ("Abstract Builder" of the Builder-Pattern)
    """

    def __init__(self):
        self.use_case = None

    def create_use_case(self):
        self.use_case = RenderResultsReporter()

