"""Default Use-Case Configurator

    (Implements Property/Setter Dependency Injection for
    configurable fields in Use-Case)
    ("Concrete Builder" of the Builder-Pattern)
"""
# STANDARD IMPORTS
import os

# LOCAL IMPORTS
from builder.use_case_builder import UseCaseBuilder


class DefaultConfigurator(UseCaseBuilder):
    """ Provides the methods for Configuring the UseCase field values"""

    def __init__(self):
        super(DefaultConfigurator, self).__init__()

    def set_cli_args(self, args):
        self.use_case.cli_args = args

    def set_business_rules(self, business_rules):
        self.use_case.business_rules = business_rules

    def set_reader(self, reader):
        self.use_case.reader = reader

    def set_render_data_directory(self, cli_arg_directory):
        data_dir = os.getcwd()
        if cli_arg_directory:
            data_dir = os.path.abspath(cli_arg_directory)
        self.use_case.render_data_directory = data_dir




