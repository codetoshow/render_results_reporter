#!/usr/local/bin/python
# """Command-Line Interface Handler

# STANDARD IMPORTS
import argparse


def get_command_line_arguments():
    """
    Parse and return the command line arguments
    """
    t_help = "output the average render time in seconds"
    c_help = "output the average peak cpu"
    m_help = "output the average ram usage."
    capital_m_help = "output the id for the render with the highest peak RAM."
    capital_c_help = "output the id for the render with the highest peak CPU"
    s_help = "output the result of all of the above commands, in the order listed above, each on a separate line"
    a_help = "filter output for only renders that use the given application"
    r_help = "filter output for only renders that use the given renderer"
    f_help = "if present, include data from failed renders in the output"
    d_help = "directory containing files of render-results"

    parser = argparse.ArgumentParser()

    parser.add_argument("-a", "--app", action="store", help=a_help)
    parser.add_argument("-r", "--renderer", action="store", help=r_help)
    parser.add_argument("-f", "--failed", action="store_true", help=f_help)
    output_group = parser.add_mutually_exclusive_group()
    output_group.add_argument("-t", "--avgtime", action="store_true", help=t_help)
    output_group.add_argument("-c", "--avgcpu", action="store_true", help=c_help)
    output_group.add_argument("-m", "--avgram", action="store_true", help=m_help)
    output_group.add_argument("-M", "--maxram", action="store_true", help=capital_m_help)
    output_group.add_argument("-C", "--maxcpu", action="store_true", help=capital_c_help)
    output_group.add_argument("-s", "--summary", action="store_true", help=s_help)
    parser.add_argument("-d", "--render_results_directory", action="store", help=d_help)

    args = parser.parse_args()

    return args






