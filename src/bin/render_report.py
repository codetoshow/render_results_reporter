#!/usr/local/bin/python
"""
Bootstrap application environment
RUN Command-Line-Interface
"""
# STANDARD IMPORTS
import argparse
import os
import sys


def _get_package_roots():
    """
    Determines and returns the paths to
    package-root and
    source-root
    """
    bin_dir = os.path.dirname(os.path.abspath(__file__))
    bin_dir_parts = bin_dir.split(os.path.sep)
    path_parts = bin_dir_parts[:-1]
    src_root = os.path.join('/', *path_parts)
    path_parts = bin_dir_parts[:-2]
    pkg_root = os.path.join('/', *path_parts)
    return pkg_root, src_root


def _setup_environment():
    """
    Sets environment variables with some root directories
    Updates sys.path
    :return:
    """
    package_root, source_root = _get_package_roots()
    os.environ["PACKAGE_ROOT"] = package_root
    os.environ["PACKAGE_CONFIG"] = package_root + "/etc/render_report_configuration.yml"
    os.environ["SRC_ROOT"] = source_root
    os.environ["DATA_ROOT"] = package_root + "/data"
    sys.path.append(source_root)


def main():
    _setup_environment()

    import cli_handler
    import launcher as launcher

    cmd_line_args = cli_handler.get_command_line_arguments()
    launcher.launch(cmd_line_args)


if __name__ == '__main__':
    main()

