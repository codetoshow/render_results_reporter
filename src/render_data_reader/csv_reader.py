"""

"""
# STANDARD IMPORTS
import os
import yaml
from pprint import pprint

# LOCAL IMPORTS
from reader_interface import ReaderInterface


class CsvReader(ReaderInterface):
    def __init__(self, prefs):
        super(ReaderInterface, self).__init__()

        self._prefs = prefs

        self._data_file = None
        self._render_results_data = {}

    def _get_csv_data_strings(self):
        with open(self._data_file, mode='r') as f:
            data = f.readlines()
        return data

    def _set_render_results_data(self):
        render_data = {}
        csv_data = self._get_csv_data_strings()
        field_names = self._prefs.get('render_data_columns')

        for line in csv_data:
            data_string = line.strip()
            data_list = data_string.split(",")

            render_data[data_list[0]] = {}
            col = 0
            for key in field_names:
                render_data[data_list[0]][key] = data_list[col]
                col += 1
        self._render_results_data.update(render_data)

    def read(self, data_file):
        self._data_file = data_file
        self._set_render_results_data()

        return self._render_results_data


if __name__ == '__main__':
    test_file = "/Users/kenmohamed/repo/code_samples_repo/bardel_task_dev/data/render_results/renders_2018-09-19.csv"
    prefs_file = "/Users/kenmohamed/repo/code_samples_repo/bardel_task_dev/etc/render_report_configuration.yml"


    reader = CsvReader(prefs_file)
    render_data = reader.read(test_file)
    pprint(render_data)




