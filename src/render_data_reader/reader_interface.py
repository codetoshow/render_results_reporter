"""Reader interface"""


class ReaderInterface(object):
    """Readers are coded to this interface
       Use-cases use this interface
    """
    def read(self, data_file):
        raise NotImplementedError

