"""
render-results filnames:

    renders_<YEAR>-<MONTH>-<DAY>.csv
    e.g.renders_2018-09-26.csv
"""
# STANDARD IMPORTS
import re


class BusinessRules(object):
    def __init__(self):
        self._filename_pattern = re.compile('^renders_\d{4}-\d{2}-\d{2}\.csv$')

    def is_valid_filename(self, name):
        if re.search(self._filename_pattern, name):
            return True
        else:
            return False

