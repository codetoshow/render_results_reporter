"""
RenderResultsReporter

Use-Case Controller.

INPUT
    csv_directory = None

  * if None, look for csv files in CWD
    else we have an absolute or relative path
  * get all files at csv_directory
  * filter for properly named files
  * READ all data
OUTPUT
  * report number of successful renders
"""
# STANDARD IMPORTS
import operator
import os


class RenderResultsReporter(object):
    """Use-Case
    (Implements the Command-Pattern)
    ("Product" of the Builder-Pattern)
    """
    def __init__(self):
        self.cli_args = None
        self.business_rules = None
        self.reader = None
        self.render_data_directory = None
        """set via setter DI (setters in builder/configurator)"""

        self._render_results_data = {}
        """{data} is extracted here from [files]"""

        self._filtered_render_results_data = {}
        self._report_type = "default"

    def _set_report_type(self):
        """Links command-line args to dispatch code in execute()"""
        if self.cli_args.avgtime:
            self._report_type = "avgtime"
            return
        if self.cli_args.avgcpu:
            self._report_type = "avgcpu"
            return
        if self.cli_args.avgram:
            self._report_type = "avgram"
            return
        if self.cli_args.maxram:
            self._report_type = "maxram"
            return
        if self.cli_args.maxcpu:
            self._report_type = "maxcpu"
            return
        if self.cli_args.summary:
            self._report_type = "summary"
            return

    def execute(self):
        """The command for this Command-Class

        * Get results files
        * Get results data
        * Filter results (directed by command-line args)
        * Get report-type (from command-line args)
        * Dispatch to appropriate reporter
        """
        # get results files

        render_results_files = [file_ for file_ in files(self.render_data_directory)
                                if self.business_rules.is_valid_filename(file_)]

        if not render_results_files:
            print "ERROR: No properly named csv files were found at {}".format(self.render_data_directory)
            return

        # get results data (from the files)
        for file_ in render_results_files:
            pathfile = os.path.join(self.render_data_directory, file_)
            data = self.reader.read(pathfile)
            if data:
                self._render_results_data.update(data)

        self._filter_results()

        self._set_report_type()  # uses cli_args

        # dispatch to appropriate reporter
        method_name = self._report_type + "_report"
        if hasattr(self, method_name):
            method = getattr(self, method_name)
            method()

    #  -- Filter Methods --  #

    def _filter_results(self):
        """Filters results data, driven by command-line arguments
        Collects UIDs corresponding to the "app"|"renderer"|"success" filters
        each filter having its own collection as class attrs
        """
        reportable_uids = set(self._get_reportable_uids())
        renderer_uids = set(self._get_renderer_filtered_uids())
        app_uids = set(self._get_app_filtered_uids())

        filtered_uids = reportable_uids & app_uids & renderer_uids

        # collect the filtered data
        if filtered_uids:
            for uid in filtered_uids:
                self._filtered_render_results_data[uid] = self._render_results_data[uid]
        else:
            self._filtered_render_results_data = self._render_results_data

    def _get_reportable_uids(self):
        """collects uids for [ALL | SUCCESSFUL] renders"""
        result = []
        if not self.cli_args.failed:
            for uid in self._render_results_data:
                success = self._render_results_data[uid].get("render_success")
                if success == "true":
                    result.append(uid)
        else:
            result = self._render_results_data.keys()
        return result

    def _get_renderer_filtered_uids(self):
        """collects uids for specified renderer"""
        result = []
        if self.cli_args.renderer:
            for uid in self._render_results_data:
                renderer = self._render_results_data[uid].get("renderer_name")
                if renderer == self.cli_args.renderer:
                    result.append(uid)
        else:
            result = self._render_results_data.keys()
        return result

    def _get_app_filtered_uids(self):
        """collects uids for specified app"""
        result = []
        if self.cli_args.app:
            for uid in self._render_results_data:
                app_name = self._render_results_data[uid].get("app_name")
                if app_name == self.cli_args.app:
                    result.append(uid)
        else:
            result = self._render_results_data.keys()
        return result

    #  -- Report Methods --  #

    def default_report(self):
        """number of successful renders"""
        successful = 0
        for uid in self._filtered_render_results_data:
            success = self._filtered_render_results_data[uid].get("render_success")
            if success == "true":
                successful += 1
        print "There were {} successful renders".format(str(successful))

    def avgtime_report(self):
        """average render time in seconds """
        milliseconds = 0
        for uid in self._filtered_render_results_data:
            milli = int(self._filtered_render_results_data[uid].get("render_time"))
            milliseconds += milli
        avg_seconds = int(milliseconds/len(self._filtered_render_results_data)/1000.0)

        print "Average Render Time: {} seconds.".format(avg_seconds)

    def avgcpu_report(self):
        """average peak cpu %"""
        cpu_usage = 0
        for uid in self._filtered_render_results_data:
            cpu = int(self._filtered_render_results_data[uid].get("max_cpu"))
            cpu_usage += cpu
        average = cpu_usage/len(self._filtered_render_results_data)

        print "Average Peak-CPU: {}%.".format(average)

    def avgram_report(self):
        """average ram usage ram"""
        ram_usage = 0
        for uid in self._filtered_render_results_data:
            this_ram_usage = int(self._filtered_render_results_data[uid].get("max_ram"))
            ram_usage += this_ram_usage
        average = ram_usage/len(self._filtered_render_results_data)

        print "Average RAM Usage: {} MB.".format(average)

    def maxram_report(self):
        """id for the render with the highest peak RAM. """
        uid_peak_ram = {}
        for uid in self._filtered_render_results_data:
            peak_ram = int(self._filtered_render_results_data[uid].get("max_ram"))
            uid_peak_ram[uid] = peak_ram
        uid = max(uid_peak_ram.iteritems(), key=operator.itemgetter(1))[0]

        print "Max Peak-RAM Render-ID: {}".format(uid)

    def maxcpu_report(self):
        """id for the render with the highest peak CPU """
        uid_peak_cpu = {}
        for uid in self._filtered_render_results_data:
            peak_cpu = int(self._filtered_render_results_data[uid].get("max_cpu"))
            uid_peak_cpu[uid] = peak_cpu
        uid = max(uid_peak_cpu.iteritems(), key=operator.itemgetter(1))[0]

        print "Max Peak-CPU Render-ID: {}".format(uid)

    def summary_report(self):
        """result of all of the above commands
         in the specific order:
        "avgtime", "avgcpu", "avgram","maxram", "maxcpu","summary"
         """
        if self._filtered_render_results_data:
            self.default_report()
            self.avgtime_report()
            self.avgcpu_report()
            self.avgram_report()
            self.maxram_report()
            self.maxcpu_report()


def files(path):
    """generates filenames found at 'path'"""
    for file_ in os.listdir(path):
        if os.path.isfile(os.path.join(path, file_)):
            yield file_
