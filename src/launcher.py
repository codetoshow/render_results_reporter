"""Entry point for render_results_reporter

Manage the build process (assembles the runtime components)
Launch use-case
"""

# LOCAL IMPORTS
from builder.use_case_builder import UseCaseAssembler
from configuration_manager import get_preferences
from render_data_reader.csv_reader import CsvReader
from builder.default_configurator import DefaultConfigurator
from business_rules import BusinessRules

ASSEMBLER = False


def launch(cli_args):
    """ render_results_reporter Access Point """
    global ASSEMBLER

    if not ASSEMBLER:
        ASSEMBLER = True

        # get concrete builder (use-case configurator)
        use_case_config_class = DefaultConfigurator

        # ASSEMBLE
        assembler = UseCaseAssembler(builder=use_case_config_class,
                                     reader=CsvReader,
                                     cli_args=cli_args,
                                     business_rules=BusinessRules,
                                     prefs=get_preferences())

        use_case_controller = assembler.get_use_case()

        # EXECUTE
        use_case_controller.execute()
